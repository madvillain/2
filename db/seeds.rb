# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

bands = Band.create([{ name: 'KISS'}, { name: 'ACDC'}, { name: 'Guns n Roses'}, { name: 'Metallica'}])

clubs = Club.create([{ name: 'ROXY', street_address: '1234 Main' }, { name: 'The Unicorn', street_address: '4321 Broadway'}])
